[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![Pipeline Status](https://gitlab.com/tawasta/odoo/crm/badges/17.0-dev/pipeline.svg)](https://gitlab.com/tawasta/odoo/crm/-/pipelines/)

Customer relationship management
================================

[//]: # (addons)

Available addons
----------------
addon | version | maintainers | summary
--- | --- | --- | ---
[crm_lead_to_sale_transfer_attachments](crm_lead_to_sale_transfer_attachments/) | 17.0.1.0.0 |  | Converting opportunities to sales copies related attachments

[//]: # (end addons)
